export const percentage = (partialValue, totalValue) => {
  return (100 * partialValue) / totalValue;
};

export const convertArrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, initialValue);
};
