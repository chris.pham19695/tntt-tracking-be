const getAllTrackingClassQuery = (year) =>
  `SELECT * FROM lop_hoc lh WHERE lh.khoa_hoc_id = ${year}`;

const getLatestSchoolYearQuery = `
SELECT kh.id
FROM khoa_hoc kh
ORDER BY id desc
LIMIT 1
`;

const getTeacheClassByYearQuery = (year) => `
                                          SELECT lh.nganh, lh.cap, lh.doi, lh.vi_tri_hoc, lh.tracking
                                          FROM tai_khoan tk
                                          LEFT JOIN taikhoan_lophoc tkl ON tk.id = tkl.tai_khoan_id
                                          LEFT JOIN lop_hoc lh ON tkl.lop_hoc_id = lh.id
                                          WHERE tk.loai_tai_khoan = 'HUYNH_TRUONG'
                                              AND tk.trang_thai = 'HOAT_DONG'
                                              AND lh.khoa_hoc_id = ${year}`;

const getTaiKhoanHTByLopHoc = (id, year) => `
SELECT tk.id, tk.ten_thanh, tk.ho_va_ten, tk.dia_chi, tk.dien_thoai, tk.email, tkl.lop_hoc_id
FROM tai_khoan tk
LEFT JOIN taikhoan_lophoc tkl ON tk.id = tkl.tai_khoan_id
WHERE tkl.lop_hoc_id in (${id})
  AND tk.trang_thai = 'HOAT_DONG'
  AND tk.loai_tai_khoan = 'HUYNH_TRUONG'
`;

const getTaiKhoanTNByLopHoc = (id, year) => `
SELECT tk.id, tk.ten_thanh, tk.ho_va_ten, tk.dia_chi, tk.dien_thoai, tkl.lop_hoc_id
FROM tai_khoan tk
LEFT JOIN taikhoan_lophoc tkl ON tk.id = tkl.tai_khoan_id
WHERE tkl.lop_hoc_id in (${id})
  AND tk.trang_thai = 'HOAT_DONG'
  AND tk.loai_tai_khoan = 'THIEU_NHI'
`;

const updateDeTailStatusTrackingQuery = (id) => `
UPDATE lop_hoc lh SET lh.tracking = 0 WHERE id = ${id};
`;

const updateAllStatusTrackingDeActiveQuery = `
UPDATE lop_hoc lh SET lh.tracking = 0;
`;

const updateAllStatusTrackingActiveQuery = `
UPDATE lop_hoc lh SET lh.tracking = 1;
`;

const getCountHSByNganh = (year) => `
SELECT lh.nganh, COUNT(tk.id) AS SL
FROM tai_khoan tk
LEFT JOIN taikhoan_lophoc tkl ON tk.id = tkl.tai_khoan_id
LEFT JOIN lop_hoc lh ON tkl.lop_hoc_id = lh.id
WHERE tk.loai_tai_khoan = 'THIEU_NHI'
    AND tk.trang_thai = 'HOAT_DONG'
    AND lh.khoa_hoc_id = ${year}
GROUP BY lh.nganh`;

const query = {
  getAllTrackingClassQuery,
  getLatestSchoolYearQuery,
  getTeacheClassByYearQuery,
  getTaiKhoanHTByLopHoc,
  updateAllStatusTrackingDeActiveQuery,
  updateAllStatusTrackingActiveQuery,
  updateDeTailStatusTrackingQuery,
  getCountHSByNganh,
  getTaiKhoanTNByLopHoc,
};

module.exports = query;
