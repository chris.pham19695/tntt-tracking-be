const server = require('./app');
const port = process.env.PORT || 8000;

server.listen(port, function () {
  console.log('Server listening on port ' + port);
});
