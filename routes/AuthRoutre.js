const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const { Loginvalidate } = require('../utils/validator');

const { login } = require('../controllers/authCtrl');

router.route('/login').post(Loginvalidate, login, completeRes);

module.exports = router;
