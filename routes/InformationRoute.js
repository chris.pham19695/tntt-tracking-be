const express = require('express');
const {
  getAllInformationTracking,
  refreshToDeActive,
  updateDetailStatus,
  getAllTotalAnalyst,
  refreshToActive,
  getAllInfoByClass,
} = require('../controllers/InformationCtrl');
const completeRes = require('../middlewares/completeRes');
const {
  UpdateStatusDetailQuery,
  DetAllInfoByClassValidate,
} = require('../utils/validator');
const router = express.Router();

router.route('/getAll').post(getAllInformationTracking, completeRes);

router.route('/refresh').post(refreshToDeActive, completeRes);

router.route('/refreshActive').post(refreshToActive, completeRes);

router
  .route('/changeStatus')
  .post(UpdateStatusDetailQuery, updateDetailStatus, completeRes);

router.route('/getTotalAnalys').post(getAllTotalAnalyst, completeRes);

router
  .route('/getAllByClass')
  .post(DetAllInfoByClassValidate, getAllInfoByClass, completeRes);

module.exports = router;
