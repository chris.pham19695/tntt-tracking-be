const express = require('express');
const router = express.Router();

router.use('/infomations', require('./InformationRoute'));
router.use('/auth', require('./AuthRoutre'));

module.exports = router;
