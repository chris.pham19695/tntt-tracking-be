const axios = require('axios');

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common.Accept = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.timeout = 60000;

const invoke = (url, data = {}, method = 'POST', loading) =>
  new Promise((resolve, reject) => {
    const reqConfig = {
      method,
      url,
      data,
    };

    // console.log('reqConfig >> ', reqConfig);
    axios(reqConfig)
      .then((res) => {
        console.log(res);
        if (!res) return;
        if (res.status === 200 || res.status === 201) {
          resolve(res.data);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });

const ApiService = {
  invoke,
};

module.exports = ApiService;
