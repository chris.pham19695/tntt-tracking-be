var express = require('express');
var path = require('path');
var logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes');

require('dotenv').config();

var app = express();

app.use(cors());
app.use(logger('dev'));

//body parser
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use(express.json());

app.use('/api', routes);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/api/test', (req, res) => {
  res.status(200).json({ message: 'ok' });
});

module.exports = app;
