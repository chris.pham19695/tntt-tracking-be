require('dotenv').config();

module.exports = {
  db_host: process.env.db_host,
  db_user: process.env.db_user,
  db_password: process.env.db_password,
  db_database: process.env.db_database,
  db_port: process.env.db_port,
  url_image: (id) => `${process.env.url_image}/${id}`,
  api_auth: process.env.api_login,
};
