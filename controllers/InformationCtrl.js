const env_var = require('../config/env_var');
const dbConnect = require('../db/Connections');

const Connections = require('../db/Connections');
const QueryExecute = require('../db/QueryExecute');

const QueryService = require('../models/dataset/query');
const ResponseModal = require('../models/model/response');

module.exports = {
  getAllInformationTracking: async (req, res, next) => {
    try {
      const conn = await Connections();

      const latestSchoolYear = await QueryExecute(
        conn,
        QueryService.getLatestSchoolYearQuery
      );
      const year = Array.isArray(latestSchoolYear) && latestSchoolYear[0]?.id;

      const data = await QueryExecute(
        conn,
        QueryService.getAllTrackingClassQuery(year)
      );
      const arr = data.map((item) => item?.id).toString();

      const listTeacher = await QueryExecute(
        conn,
        QueryService.getTaiKhoanHTByLopHoc(arr, year)
      );

      conn.end();

      const arrNew = [];
      let obj = {};
      for (let index = 0; index < data.length; index++) {
        const arrTecher = [];
        for (let k = 0; k < listTeacher.length; k++) {
          if (data[index].id === listTeacher[k]?.lop_hoc_id) {
            arrTecher.push(listTeacher[k]);
            obj = {
              ...data[index],
              teachers: arrTecher,
            };
          }
        }
        arrNew.push(obj);
      }

      //console.log(arrNew);
      //  conn.release();
      req.status = 200;
      req.return = {
        ...ResponseModal,
        message: 'Success !',
        data: [...arrNew],
      };
      next();
    } catch (error) {
      req.status = 404;
      req.return = { message: 'Failed!', error };
      next();
    } finally {
    }
  },

  updateDetailStatus: async (req, res, next) => {
    const detailObj = req.body;
    try {
      const conn = await Connections();
      const data = await QueryExecute(
        conn,
        QueryService.updateDeTailStatusTrackingQuery(detailObj.id)
      );
      conn.end();
      req.status = 200;
      req.return = { ...ResponseModal, message: 'Success !', data };
      next();
    } catch (error) {
      req.status = 400;
      console.log(error);
      req.return = { message: 'Failed!', error };
      next();
    }
  },

  refreshToDeActive: async (req, res, next) => {
    try {
      const conn = await Connections();
      const data = await QueryExecute(
        conn,
        QueryService.updateAllStatusTrackingDeActiveQuery
      );
      conn.end();
      req.status = 200;
      req.return = { ...ResponseModal, message: 'Success !', data };
      next();
    } catch (error) {
      req.status = 400;
      console.log(error);
      req.return = { message: 'Failed!', error };
      next();
    }
  },

  refreshToActive: async (req, res, next) => {
    try {
      const conn = await Connections();
      const data = await QueryExecute(
        conn,
        QueryService.updateAllStatusTrackingActiveQuery
      );
      conn.end();
      req.status = 200;
      req.return = { ...ResponseModal, message: 'Success !', data };
      next();
    } catch (error) {
      req.status = 400;
      console.log(error);
      req.return = { message: 'Failed!', error };
      next();
    }
  },

  getAllTotalAnalyst: async (req, res, next) => {
    try {
      const conn = await Connections();
      const latestSchoolYear = await QueryExecute(
        conn,
        QueryService.getLatestSchoolYearQuery
      );

      const year = Array.isArray(latestSchoolYear) && latestSchoolYear[0]?.id;
      const data = await QueryExecute(
        conn,
        QueryService.getCountHSByNganh(year)
      );

      const dataLastYear = await QueryExecute(
        conn,
        QueryService.getCountHSByNganh(year - 1)
      );
      conn.end();
      req.status = 200;
      req.return = {
        ...ResponseModal,
        message: 'Success !',
        data,
        dataLastYear,
        year,
      };
      next();
    } catch (error) {
      req.status = 400;
      console.log(error);
      req.return = { message: 'Failed!', error };
      next();
    }
  },

  getAllInfoByClass: async (req, res, next) => {
    const detailObj = req.body;
    try {
      const conn = await Connections();

      const dataHT = await QueryExecute(
        conn,
        QueryService.getTaiKhoanHTByLopHoc(detailObj?.id)
      );

      const dataTN = await QueryExecute(
        conn,
        QueryService.getTaiKhoanTNByLopHoc(detailObj?.id)
      );
      conn.end();
      req.status = 200;
      req.return = {
        ...ResponseModal,
        message: 'Success !',
        dataHT:
          Array.isArray(dataHT) &&
          dataHT.map((item) => ({
            ...item,
            image: env_var.url_image(item?.id),
          })),
        dataTN:
          Array.isArray(dataTN) &&
          dataTN.map((item) => ({
            ...item,
            image: env_var.url_image(item?.id),
          })),
      };
      next();
    } catch (error) {
      req.status = 400;
      console.log(error);
      req.return = { message: 'Failed!', error };
      next();
    }
  },
};
