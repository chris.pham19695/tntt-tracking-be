const ResponseModal = require('../models/model/response');
const env_var = require('../config/env_var');
const ApiService = require('../services/api.service');

module.exports = {
  login: async (req, res, next) => {
    try {
      const { username, password } = req.body;
      const obj = {
        id: username,
        password: password,
      };

      const rs = await ApiService.invoke(env_var.api_auth, { ...obj });
      console.log(111, rs);

      req.status = 200;
      req.return = { ...ResponseModal, message: 'Success !', data: rs?.data };
      next();
    } catch (error) {
      const rs = error?.response;
      req.status = 200;
      req.return = {
        ...ResponseModal,
        message: rs?.data?.error,
        success: false,
      };
      next();
    }
  },
};
