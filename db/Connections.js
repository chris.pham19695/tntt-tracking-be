const mysql = require('mysql');
const util = require('util');
const env_var = require('../config/env_var');

const connection = async () =>
  new Promise((resolve, reject) => {
    const connection = mysql.createConnection({
      host: env_var.db_host,
      user: env_var.db_user,
      password: env_var.db_password,
      database: env_var.db_database,
      port: env_var.db_port,
    });
    connection.connect((error) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(connection);
    });
  });

module.exports = connection;
